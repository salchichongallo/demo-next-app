#!/bin/sh

for variable in $(env | grep '^NEXT_PUBLIC_' | cut -d '=' -f 1); do
  placeholder="#{${variable}}"
  value=$(eval echo \$$variable)
  find $APP_PATH -type f -exec sed -i "s/${placeholder}/${value}/g" {} +
done
