import Link from 'next/link';

export default function Header() {
  return (
    <header className="p-4">
      <nav className="flex items-center justify-between">
        <Link href="/">Inicio</Link>
        <Link href="/auth/login">Login</Link>
      </nav>
    </header>
  );
}
