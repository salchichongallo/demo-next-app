'use client';

export default function Home() {
  return (
    <div>
      <h1 className="text-4xl mb-4 font-semibold">Ejemplo</h1>
      <p>Bienvenido al ejemplo de Next.js</p>
    </div>
  );
}
